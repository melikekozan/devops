# devops
Öncelikle VirtualBox kurulumunu yapıp yeni sanal makine yaratıyoruz.
Centos7 kurulumunu tamamladıktan sonra makinemizi ayağa kaldırıyoruz.

Güncellemeleri kotrol etmek için

$yum update -y 

komutunu çalıştrıp güncellemeleri alıyoruz.Ardından makinemiz üzerinde yeni bir kullanıcı oluşturmak için;

$sudo useradd melike.kozan

parola tanımlamak için;

$sudo passwd melike.kozan

oluşturduğumuz kullanıcıya geçiş için;
 
 $su melike.kozan

 Bu işlemlerden sonra disk genişletmek için vbox üzerinden Centos makinemizi yapılandırıyoruz. 
 yeni bir disk ekledikten sonra terminalimize geçip sırasıyla;

$ldbl
disklerimizi görüntüledik

$sudo fdisk /dev/sdb
gidip command aksiyonlarından uygun olanları seçtim.

$mkfs.ext4 /dev/sdb1
$mkdir -p /opt/bootcamp
$mount /dev/sdb1 /opt/bootcamp

boş bir dizin oluşturup diskimi oraya mount ettim.

ardından 
$cd /opt/bootcamp/
altına gidip

$touch bootcamp.txt
$echo "merhaba trendyol"> bootcamp.txt

bootcamp file yaratıp içerisine merhaba trendyol yazdırdım.

son olarak da root dizininde 

$sudo find / -type f -name "bootcamp.txt" -exec mv -t /bootcamp/{}

komutunu çalıştırdım.




























